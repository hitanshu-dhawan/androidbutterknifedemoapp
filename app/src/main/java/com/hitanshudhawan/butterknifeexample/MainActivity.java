package com.hitanshudhawan.butterknifeexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private int count = 0;

    @BindView(R.id.text_view)
    TextView textView;

    @BindView(R.id.button)
    Button button;

    @BindString(R.string.click)
    String clickMeString;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    List<String> strings;
    MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        button.setText(clickMeString);

        strings = new ArrayList<>();
        myAdapter = new MyAdapter(this,strings);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        strings.add("one");
        strings.add("two");
        strings.add("three");
        strings.add("four");
        strings.add("five");

        myAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.button)
    public void click() {
        textView.setText(String.valueOf(++count));
    }
}
